# Esto es un bono
Prueba sobre lo explicado recientemente sobre el lenguaje ***Markdown***.

Algunos entornos de desarrollo para markdown son los siguientes:
1. **Online:**
    * Editor.md
2. **Local:**
    * [**Atom**](https://atom.io/)
    * Visual Studio Code
    * Sublime text

El logo de Markdown es el siguiente:

![](https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Markdown-mark.svg/1200px-Markdown-mark.svg.png)

----


## Primer previo
| Asunto | Nota |
| :----------------- | -: |
|  Parcial teórico |  4 |
|  Parcial práctico |  3 |
|  Bono |  0.2 |

-------

Para poder realizar un commit a un proyecto de gitlab se debe usar la siguiente sintaxis

`git commit -m "texto"`

> Las comillas ("") se pueden omitir si el mensaje del commit es de solo una palabra.

